﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace web_project.Models
{
    public class Compare
    {
        public float confidence { get; set; }
        public FaceDetect image1_face { get; set; }
        public FaceDetect image2_face { get; set; }
    }

    public class FaceDetect
    {
        public FaceRectangle FaceRectangle { get; set; }
        public float quality { get; set; }
    }

    public class FaceRectangle
    {
        public int x { get; set; }
        public int y { get; set; }
        public int height { get; set; }
        public int width { get; set; }
    }
}
